from flask import Flask, make_response, render_template, request, redirect
import io
import csv
import os
import sqlite3
import database
from sqlite3 import Error
from database import Database


app = Flask(__name__)
db = Database()
db.create_table()


@app.route('/', methods=['GET'])
@app.route('/home', methods=['GET'])
def home():
    db.view()
    rows = db.view()
    return render_template('home.html', rows=rows)


@app.route('/save', methods=['POST'])
def save():
    user_csv = request.form.get('data_file')
    with open(user_csv, 'r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        db.insert(csv_reader)

    return redirect('/')


@app.route('/delete', methods=['POST'])
def delete():
    id = request.form.get('id')
    db.delete(id)
    return redirect('/')


@app.route('/update', methods=['POST'])
def update():
    id = request.form.get('id')
    rows = db.select(id)
    db.select(id)
    return render_template('update.html', rows=rows)


@app.route('/saveupdate', methods=['POST'])
def saveupdate():
    id = request.form.get('id')
    first_name = request.form.get('first_name')
    last_name = request.form.get('last_name')
    email = request.form.get('email')
    gender = request.form.get('gender')
    ip_address = request.form.get('ip_address')

    db.update(id, first_name,
              last_name, email, gender, ip_address)

    return redirect('/')


@app.route('/download', methods=['GET'])
def download():
    fieldnames = ['id', 'first_name', 'last_name',
                  'email', 'gender', 'ip_address']
    db.view()
    rows = db.view()
    results = []
    for row in rows:
        results.append(dict(zip(fieldnames, row)))

    si = io.StringIO()
    writer = csv.DictWriter(si, fieldnames=fieldnames)
    writer.writeheader()
    for line in results:
        writer.writerow(line)

    response = make_response(si.getvalue())
    response.headers['Content-Disposition'] = 'attachment; filename=export.csv'
    response.headers["Content-type"] = "text/csv"
    return response


@app.route('/add', methods=['GET'])
def add():
    return render_template('add.html')


@app.route('/saveadd', methods=['POST'])
def saveadd():
    first_name = request.form.get('first_name')
    last_name = request.form.get('last_name')
    email = request.form.get('email')
    gender = request.form.get('gender')
    ip_address = request.form.get('ip_address')
    db.add_data(first_name, last_name,
                email, gender, ip_address)
    return redirect('/')


@app.route('/insertfile', methods=['GET'])
def insertfile():
    return render_template('insert.html')


if __name__ == '__main__':
    app.run(debug=True)
