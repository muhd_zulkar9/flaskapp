from sqlite3 import Error
import os
import sqlite3


class Database:
    conn = None

    def __init__(self):
        self.conn = sqlite3.connect('testing2.db', check_same_thread=False)
        self.cur = self.conn.cursor()

    def create_table(self):
        self.cur.execute(""" CREATE TABLE IF NOT EXISTS testing_table (
                        "id" INTEGER PRIMARY KEY,   
                        "first_name" TEXT,
                        "last_name" TEXT,   
                        "email" TEXT,
                        "gender" TEXT,
                        "ip_address" REAL
                        );""")
        self.conn.commit()

    def insert(self, csv_reader):
        self.cur = self.conn.cursor()
        sql = """INSERT INTO testing_table(first_name, last_name, email, gender, ip_address) 
        VALUES(:first_name, :last_name, :email, :gender, :ip_address)"""
        self.cur.executemany(sql, csv_reader)
        self.conn.commit()

    def view(self):
        self.cur = self.conn.cursor()
        self.conn.row_factory = sqlite3.Row
        self.cur.execute("SELECT * from testing_table")
        rows = self.cur.fetchall()
        self.conn.commit()
        return rows

    def delete(self, id):
        self.cur = self.conn.cursor()
        self.cur.execute("DELETE from testing_table WHERE id = ?", (id,))
        self.conn.commit()

    def select(self, id):
        self.cur = self.conn.cursor()
        self.conn.row_factory = sqlite3.Row
        self.cur.execute("SELECT * from testing_table WHERE id = ?", (id,))
        rows = self.cur.fetchall()
        self.conn.commit()
        return rows

    def update(self, id, first_name, last_name, email, gender, ip_address):
        self.cur = self.conn.cursor()
        self.cur.execute("""Update testing_table
                    SET
                    id = ?,
                    first_name = ?,
                    last_name = ?,
                    email = ?,
                    gender = ?,
                    ip_address= ?
                    WHERE id = ?""", (id, first_name, last_name, email, gender, ip_address, id))
        self.conn.commit()

    def add_data(self, first_name, last_name, email, gender, ip_address):
        self.cur = self.conn.cursor()
        self.cur.execute('INSERT INTO testing_table(first_name, last_name, email, gender, ip_address) VALUES(?,?,?,?,?)',
                         (first_name, last_name, email, gender, ip_address))
        self.conn.commit()

    # def download(self):
    #     self.columns = [column[0] for column in self.cur.description]
    #     self.results = []
    #     self.cur = self.conn.cursor()
    #     self.conn.row_factory = sqlite3.Row
    #     self.cur.execute("SELECT * from testing_table")
    #     for row in self.cur.fetchall():
    #         self.results.append(dict(zip(self.columns, row)))
    #     self.conn.commit()

    def disconnect(self):
        self.conn.close()
